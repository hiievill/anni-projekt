

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ValikvastustegaTest{
	
	static List<Question> k�simused = new ArrayList<Question>();
	static int k�simusNo;
	
	public static void failistLugemine(String failinimi) {
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(
					new FileInputStream(new File(failinimi))));
			String rida;
			while((rida = br.readLine())!=null) {
				String[] t�kid = rida.split(" ; ");
				if (t�kid.length < 3) { 
					k�simused.add(new ShortAnswerQuestion(t�kid[0], t�kid[1])); 
					//aga kui on vigane rida ja ainult �ks t�kk? V�iks kontroll olla. J�rgmistel ka.
				} else if (t�kid[1].equals("")) {
					k�simused.add(new TrueFalseQuestion(t�kid[0],t�kid[2]));
				} else {
					k�simused.add(new MultipleChoiceQuestion(t�kid[0], t�kid[1], t�kid[2]));
				}
			}
				br.close();
		} catch (IOException e) {
			System.out.println("Sellise nimega faili ei ole.");
		}		
	}
	
	public static void vastuseKontroll (String kasutajaVastus) {
		if (k�simused.get(k�simusNo).correctAnswer.equals(kasutajaVastus)) {
			//parem oleks equals -> equalsIgnoreCase
			System.out.println("Vastus on �ige");
		} else {
			System.out.println("Vastus on vale"); 
		}
	}
	
	public static String variandidToString(MultipleChoiceQuestion k�simus) {
		List<String> a = new ArrayList<String>(Arrays.asList((k�simus.choices).split("/")));
		return "1. "+a.get(0)+" 2. "+a.get(1)+" 3. "+a.get(2);
	}
	
	public static void v�ljastaK�simus() {
		System.out.println((k�simused.get(k�simusNo)).question);
		if (k�simused.get(k�simusNo) instanceof MultipleChoiceQuestion) {
			System.out.println(variandidToString((MultipleChoiceQuestion)k�simused.get(k�simusNo)));
		}
	}
	
	
	//selle meetodi tegin kapitaalselt ringi:
	public static void kirjutaHtml(String failinimi) throws IOException {
		
		//FileWriteri asendasin BufferedWriteriga, et kodeeringut ka arvestataks
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(new File(failinimi)),"UTF-8"));
		StringBuffer sb=new StringBuffer();
		
		//HTMLi algus oli puudu:
		sb.append("<html>"+"\r\n");
		sb.append("<head>"+"\r\n");
		sb.append("<meta charset=\"UTF-8\">"+"\r\n");
		sb.append("<title>K�simused</title>"+"\r\n");
		sb.append("</head>"+"\r\n");
		sb.append("<body>"+"\r\n");
		sb.append("<h1>K�simused</h1>"+"\r\n"+"\r\n");
		sb.append("<form action=\"mailto:someone@example.com\" method=\"post\" enctype=\"text/plain\">"+"\r\n");
		
		for (int i=0;i<k�simused.size();i++){
			sb.append(k�simused.get(i).getHtml().toString());
			sb.append("\r\n");
		}
		
		//HTMLi l�pp oli puudu
		sb.append("<br/>"+"\r\n");
		sb.append("<input type=\"submit\" value=\"Saada vastused\" />"+"\r\n");
		sb.append("</form>"+"\r\n"+"\r\n");
		sb.append("</body>"+"\r\n");
		sb.append("</html>");
		
		bw.write(sb.toString());
		bw.close();
	}
	
	public static String getKasutajaVastus(BufferedReader br) {
		String kasutajaVastus = "";
		try {
			kasutajaVastus = br.readLine();
		} catch (IOException e) {
			System.out.println("Vastust ei sisestatud");
		}
		return kasutajaVastus;
	}
    
	public static void main(String[] args) throws IOException {
		if (args.length>0){
			failistLugemine(args[0]);
			
			//originaalis k�sis ainult 1. k�simuse, t�stsin ts�klisse:
			BufferedReader br = new BufferedReader(new InputStreamReader(
					System.in)); //selle t�stsin ka meetodist getKasutajaVastus v�lja, muidu ei t��tanud.
			for(int i=0;i<k�simused.size();i++){ 
				v�ljastaK�simus();
				String vastus=getKasutajaVastus(br);
				vastuseKontroll(vastus);
				k�simusNo++; //ilma selle reata k�sis kogu aeg ainult 1. k�simust
			}
			br.close();
		}

		
		if (args.length > 1) { 
			kirjutaHtml(args[1]);
			
		}
		

	}	
}